.section .text
.globl _start

# This subroutine exits the program, a7=the code for the exit() syscall. Zero is the exit code, ecall=x86 int 0x80
exit:
	li a0,0
	li a7,93
	ecall
	
_start:
	/*https://gitlab.com/strace/strace/blob/master/linux/64/syscallent.h */
        # This is our main code loop
        L1:
            # make space for a byte on the stack
            addi sp,sp,-8

            # Make a copy of the pointer in a1
            mv a1,sp
            # Length of input
            li a2,1

            # The getch subroutine, the a1 and a2 make sense if you read that routine
            call getch

            
            # getch places the character on the stack
            lb a1,0(sp)
            # Getch reads ascii strings, ascii "0" = literal 48. Have to subtract 48 from everything. ascii 49="1". Use `man ascii`, it will show u.
            addi a2,a1,-48

            # Store the de-asciified version back on the stack for later computation, overwriting the asciified version 
            sb a2,0(sp)

            # this code tests if a '\n' was entered, if so, no more expressions follow
            li t0,10
            beq a1,t0,print_stack

            # if it's a +, pop two items from the stack, and add them
            #43='+'
            li t0,43
            # if a1 ISNT a +, just go on to the "sub" subroutine
            bne a1,t0,sub
                lb t1,8(sp)
                lb t2,16(sp)

                # one for the operator, one for the digit we r removing
                addi sp,sp,16
                # do the math
                add t3,t1,t2
                sb t3, 0(sp)
                addi a6,a6,-8
            sub:
            # 45='-'
            li t0,45
            # If a1 ISNT a -, go on to the mul routine
            bne a1,t0,mul
                lb t1,8(sp)
                lb t2,16(sp)
                # Getch reads ascii strings, ascii "0" = literal 48. Have to subtract 48 from everything. ascii 49="1". Use `man ascii`, it will show u.
                #addi t1,t1,-48
                #addi t2,t2,-48

                # one for the operator, one for the digit we r removing
                addi sp,sp,16
                # do the math, reversing the direction because dat make sense
                sub t3,t2,t1
                sb t3, 0(sp)
                addi a6,a6,-8
            mul:
            # guess which one this is, you get the point
            li t0,42
            bne a1,t0,div
                lb t1,8(sp)
                lb t2,16(sp)
                # Getch reads ascii strings, ascii "0" = literal 48. Have to subtract 48 from everything. ascii 49="1". Use `man ascii`, it will show u.
                #addi t1,t1,-48
                #addi t2,t2,-48

                # one for the operator, one for the digit we r removing
                addi sp,sp,16
                # do the math
                mul t3,t1,t2
                sb t3, 0(sp)
                addi a6,a6,-8
            div:
            #its a /
            li t0,47
            bne a1,t0,after_op
                lb t1,8(sp)
                lb t2,16(sp)
                # Getch reads ascii strings, ascii "0" = literal 48. Have to subtract 48 from everything. ascii 49="1". Use `man ascii`, it will show u.
                #addi t1,t1,-48
                #addi t2,t2,-48

                # one for the operator, one for the digit we r removing
                addi sp,sp,16
                # do the math
                div t3,t2,t1
                sb t3, 0(sp)
                addi a6,a6,-8

            


            # if this wasn't any kind of operator, add 8 to a6, and start the loop over again. a6 is the counter for how many items are on the stack(useful later)
            after_op:
                addi a6,a6,8
                j L1



        print_stack:
            # top of the stack(no newlines)
            lb t3, 8(sp)

            li t4,10

            # This is pretty complicated math, look at the image in img/math.png
            rem t0,t3,t4
            sub t1,t3,t0
            div t1,t1,t4



            addi t0,t0,48
            addi t1,t1,48

            li a2,1

            addi a1,sp,8

            sb t1, 0(a1)
            call putstr

            sb t0, 0(a1)
            call putstr

            # This just puts a newline
            j newline
            



            
    


newline:
    lui a1,%hi(msg)
    addi a1,a1,%lo(msg)
    #length of msg
    li a2,1
    call putstr

    j exit

# address to load output into(a1)
# length of buffer(a2)
getch:
        # We overwrite addresses, so we need to save them on the stack and pop them later
        addi sp,sp,-16
        sb a7,0(sp)
        sb a0,8(sp)
	li a0,0
	li a7,63

	ecall
        lb a7, 0(sp)
        lb a0, 8(sp)
        addi sp,sp,16
        ret

    
    
#address in a1, length in a2
putstr:
        # We overwrite addresses, so we need to save them on the stack and pop them later
        addi sp,sp,-16
        sb a7,0(sp)
        sb a0,8(sp)

        li a7,64
        li a0,1

        ecall
        lb a7, 0(sp)
        lb a0, 8(sp)
        addi sp,sp,16

        ret

.section .rodata
msg:
    .string "\n"
