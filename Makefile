all:
	riscv64-linux-gnu-as main.s -o main.o
	riscv64-linux-gnu-ld main.o 
#run gdb using target remote localhost:9000
z: all
	qemu-riscv64 a.out
sz: all
	qemu-riscv64 -strace a.out
r: all
	qemu-riscv64 -strace -g 9000 a.out
g:
	riscv64-linux-gnu-gdb -q a.out
