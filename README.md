# What is this

It is going to be a reverse polish notation calculator written in risc-v assembly for the linux platform. This is the best code example of risc-v code anywhere that I can find.

## How did I learn this

Computer organization and design (risc-v edition) is a great resource on risc-v code. It is worth the money. Seriously. Not kidding. There are almost no tutorials/resources online, so trying to find helpful stuff is pretty hard. 

Since I started risc-v there seem to be better examples online, here are a few:

https://inst.eecs.berkeley.edu/~cs250/fa10/handouts/tut3-riscv.pdf
https://content.riscv.org/wp-content/uploads/2018/05/15.20-15.55-18.05.06.VEXT-bcn-v1.pdf


Manuals are great if you have high-level knowledge of the subject at hand, but stink when you are trying to learn something for the first time. 

## How to run

The makefile should help. There are a couple different rules defined: 

* `make` builds the code
* `make z` runs the code
* `make sz` runs the code and strace
* `make r` starts the program and a gdb _SERVER_
* `make g` starts gdb in a special way(You have to run `target remote :9000` to actually connect)

## Comments

Should be in the main.s file once the project is completed

## Deps

One needs a 64 bit riscv linux toolchain(maybe the elf one is satisfactory, but I haven't tested it). The names of the `as` program vary with platform (I'm on archlinux). Edit the makefile as necessary. The other thing needed is a 1000$ hifive linux board or a zero dollar installation of qemu. DO NOT USE THE system `qemu-system-riscv64` one however, because that emulates a full system and we just need the regular qemu. You also might have to change the name of qemu in the makefile because nobody can agree on anything. 


## Code styling

I haven't seen much risc-v code, so I'm just doing what makes sense to me. If there is a better way to do something, please alert me to it (preferably by issues) :)

## Using

Here are some example expressions to try

323++

32-

242/-

The calculator supports multiplication, division, addition/subtraction.

